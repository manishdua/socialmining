﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SocialMediaMining.Startup))]
namespace SocialMediaMining
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
